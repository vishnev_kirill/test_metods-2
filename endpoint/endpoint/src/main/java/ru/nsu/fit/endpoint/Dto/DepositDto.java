package ru.nsu.fit.endpoint.Dto;

/**
 * Created by kirill on 16.11.2016.
 */
public class DepositDto {

    int id;
    int money;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }
}
