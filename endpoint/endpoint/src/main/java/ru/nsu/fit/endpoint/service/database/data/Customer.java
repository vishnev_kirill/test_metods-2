package ru.nsu.fit.endpoint.service.database.data;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ru.nsu.fit.endpoint.Dto.CustomerDto;
import ru.nsu.fit.endpoint.Exceptions.*;


import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
@Entity
public class Customer extends MyEntity {

    public static final String INCORRECT_LEN_Of_FIRSTNAME="FirstName length should be more or equal 2 symbols and less or equal 12 symbols";
    public static final String INCORRECT_FORMAT_Of_FIRSTNAME="Incorrect format of firstName";
    public static final String NULL_FIRSTNAME="Not a firstName";
    public static final String INCORRECT_LEN_Of_LASTNAME="LastName length should be more or equal 2 symbols and less or equal 12 symbols";
    public static final String INCORRECT_FORMAT_Of_LASTNAME="Incorrect format of lastName";
    public static final String NULL_LASTNAME="Not a lastName";

    public static final String INCORRECT_LOGIN ="incorrect format of login";
    public static final String NULL_LOGIN="Not a login";

    public static final String INCORRECT_LEN_Of_PASSWORD = "Password's length should be more or equal 6 symbols and less or equal 12 symbols";
    public static final String INCORRECT_PASSWORD ="incorrect format of password";
    public static final String NULL_PASSWORD="Not a password";

    public static final String NEGATIVE_MONEY = "Negative amount of money";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customer_id")
    private int id;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String firstName;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String lastName;
    /* указывается в виде email, проверить email на корректность */
    @Column(unique = true)
    private String login;
    /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
    private String pass;
    /* счет не может быть отрицательным */
    private int money;

    @OneToMany(mappedBy = "customer",fetch = FetchType.EAGER)
    private Collection<User> users= new HashSet<User>();

    public Collection<User> getUsers() {
        return users;
    }

    public void setUsers(Collection<User> users) {
        this.users = users;
    }




    public Customer(String firstName, String lastName, String login, String pass, int money)
    {

        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.pass = pass;
        this.money = money;
        validate(firstName, lastName, login, pass, money);
    }

    public Customer(){

    }
    @OneToMany(mappedBy = "mycustomer", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private Collection<Subscription> subscriptions = new HashSet<Subscription>();

    public Collection<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(Collection<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    private static void validate(String firstName, String lastName, String login, String pass, int money) throws MyExcepton {
        validateFirstName(firstName);
        validateLastName(lastName);
        validateLogin(login);
        validatePasssword(pass,firstName,lastName, login);
        validateMoney(money);
    }
    private static void validateFirstName(String firstName) throws MyExcepton{
        if(firstName!=null){
            if(!(firstName.length()>=2 && firstName.length()<13))
                throw new InvalidCustomerNameException(INCORRECT_LEN_Of_FIRSTNAME);
            if(!firstName.matches("[A-Z][a-z]*")){
                throw new InvalidCustomerNameException(INCORRECT_FORMAT_Of_FIRSTNAME);
            }
        }else
            throw new InvalidCustomerNameException(NULL_FIRSTNAME);

    }

    private static void validateLastName(String lastName) throws MyExcepton{

        if(lastName!=null){
            if(!(lastName.length()>=2 && lastName.length()<13)) {
                throw new InvalidCustomerLastNameException(INCORRECT_LEN_Of_LASTNAME);
            }
            if(!lastName.matches("[A-Z][a-z]*")){
                throw new InvalidCustomerLastNameException(INCORRECT_FORMAT_Of_LASTNAME);
            }
        }else
            throw new InvalidCustomerLastNameException(NULL_LASTNAME);

    }
    private static void validateLogin(String login) throws MyExcepton{
        if(login!=null){
            if(!login.contains("@")){//regex
                throw new InvalidLoginException(INCORRECT_LOGIN);
            }
        } else
            throw new InvalidLoginException(NULL_LOGIN);

    }
    private static void validatePasssword(String pass,String firstName, String lastName, String login) throws MyExcepton{
        if(pass!=null){
            if(!(pass.length()>=6 && pass.length()<13))
                throw new InvalidPasswordException(INCORRECT_LEN_Of_PASSWORD);
            if((pass.contains(firstName)) || (pass.contains(lastName)) || (pass.contains(login))){
                throw new InvalidPasswordException(INCORRECT_PASSWORD);
            }
        }else
            throw new InvalidPasswordException(NULL_PASSWORD);


    }
    private static void validateMoney(int money) throws MyExcepton{
        if(money < 0)
            throw new MoneyException(NEGATIVE_MONEY);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) throws MyExcepton
    {
        validateFirstName(firstName);
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) throws MyExcepton {

        validateLastName(lastName);
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) throws MyExcepton {

        validateLogin(login);
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) throws MyExcepton {

        validatePasssword(pass, this.getLastName(), this.getLastName(), this.getLogin());
        this.pass = pass;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) throws MyExcepton {
        validateMoney(money);
        this.money = money;
    }

    public void setData(CustomerDto dto) throws MyExcepton{
        validate(dto.getFirstname(),dto.getLastName(),dto.getLogin(), dto.getPass(),dto.getMoney());
        this.firstName=dto.getFirstname();
        this.lastName=dto.getLastName();
        this.login=dto.getLogin();
        this.pass=dto.getPass();
        this.money=dto.getMoney();

    }

    public CustomerDto getData()  throws  MyExcepton{
        CustomerDto dto= new CustomerDto();
        dto.setId(this.getId());
        dto.setFirstname(this.firstName);
        dto.setLastName(this.lastName);
        dto.setLogin(this.login);
        dto.setPass("***");
        dto.setMoney(this.money);
        return dto;
    }
}
