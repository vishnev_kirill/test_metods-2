package ru.nsu.fit.endpoint.Dto;

/**
 * Created by kirill on 17.11.2016.
 */
public class UserSubscriptionDto {

    int customerId;
    int subscriptionId;
    int userId;


    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(int subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
