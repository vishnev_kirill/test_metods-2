package ru.nsu.fit.endpoint.mock;

import ru.nsu.fit.endpoint.Dto.BuyDto;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.DataService;
import ru.nsu.fit.endpoint.service.database.data.ServicePlan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;

import javax.persistence.EntityManager;
import javax.ws.rs.core.Response;
import java.util.Collection;

/**
 * Created by kirill on 16.11.2016.
 */
public class PlanRegistrator {


    public static String register(BuyDto dto, DataService service){
        Customer cust = service.getEntity(Customer.class, dto.getCustomerId());
        Subscription s=null;
        if (cust == null)
            return "Not Customer";

        ServicePlan plan = service.getEntity(ServicePlan.class, dto.getPlanId());
        if (plan == null)
            return "Not Plan";

        s= new Subscription();
        s.setCustomer(cust);
        s.setServicePlan(plan);
        s.setUsedSeats(0);
        s.setStatus(Subscription.Status.PROVISION);


        Collection<Subscription> subscriptions = cust.getSubscriptions();
        if(subscriptions.size()==0){
            if (plan.getName().charAt(0)!='!'){
                s.setStatus(Subscription.Status.DONE);
                service.Add(s);
                return"Plan added";
        }else{
                service.Add(s);
                Runnable r= new Mock(service,s,subscriptions, plan);
                Thread t=new Thread(r);
                t.start();
                return "you plan is provision ";
            }
        }else{
            if(plan.getName().charAt(0)!='!'){
                for(Subscription sub : subscriptions){
                    int max=sub.getServicePlan().getMaxSeats();
                    if(max==plan.getMaxSeats())
                        return "you subscriptions not compatible";
                }
                s.setStatus(Subscription.Status.DONE);
                service.Add(s);
                return"Plan added";
            }
            service.Add(s);
            Runnable r= new Mock(service,s,subscriptions, plan);
            Thread t=new Thread(r);
            t.start();
            return "you plan is provision ";

        }

    }


}
