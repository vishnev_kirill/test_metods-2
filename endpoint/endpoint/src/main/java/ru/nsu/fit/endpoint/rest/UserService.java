package ru.nsu.fit.endpoint.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.nsu.fit.endpoint.Dto.CustomerDto;
import ru.nsu.fit.endpoint.Dto.SubscriptionDto;
import ru.nsu.fit.endpoint.Dto.UserDto;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.DataService;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.data.User;

import javax.activation.DataSource;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by kirill on 12.11.2016.
 */

@DeclareRoles({"ADMIN", "CUSTOMER","USER"})
@Path("/user")
public class UserService implements Serializable {

    @Inject
    DataService service;

    @POST
    @RolesAllowed("USER")
    @Path("/getId")
    @Produces("application/json")
    public Response GetIdByLogin(String login) {
        List<User>  cust=service.getAllUsers();
        int id=0;
        for(User c: cust){
            if(c.getLogin().equals(login))
                id=c.getId();
        }
        if(id==0)
            return Response.status(402).entity("No user in system").build();


        return Response.status(200).entity(id).build();

    }


    @POST
    @RolesAllowed("USER")
    @Path("/getMySubscriptions")
    @Consumes("application/json")
    public Response getMySubscriptyions(String i) {
       int id=Integer.parseInt(i);
       User user=service.getEntity(User.class,id);
        if(user==null)
            return Response.status(401).entity("Not User").build();
        Collection<Subscription> subscriptions =user.getSubscriptions();


        ObjectMapper m= new ObjectMapper();
        List<SubscriptionDto> dto= new ArrayList<SubscriptionDto>();
        for(Subscription c  : subscriptions){
            dto.add(c.getData());

        }

        String data=null;
        try {
            data = m.writeValueAsString(dto);
        }catch (Exception e){
            return Response.status(402).entity(e.getMessage()).build();
        }
        return Response.status(200).entity(data).build();

    }

    @GET
    @RolesAllowed("USER")
    @Path("/getMyCustomer")
    @Consumes("application/json")
    public Response getMyCustomer(String i) {
        int id=Integer.parseInt(i);
        User user=service.getEntity(User.class,id);
        if(user==null)
            return Response.status(401).entity("Not User").build();

        Customer customer =user.getCustomer();
        CustomerDto dto=customer.getData();

        ObjectMapper m= new ObjectMapper();
        String data=null;
        try {
            data = m.writeValueAsString(dto);
        }catch (Exception e){
            return Response.status(402).entity(e.getMessage()).build();
        }
        return Response.status(200).entity(data).build();

    }

    @POST
    @RolesAllowed("USER")
    @Path("/geMyData")
    @Produces("application/json")
    public Response getMyData(String i) {
        int id=Integer.parseInt(i);
        User user = service.getEntity(User.class, id);
        if (user == null)
            return Response.status(401).entity("Not user").build();
        UserDto dto = user.GetData();
        ObjectMapper m = new ObjectMapper();
        String data = null;
        try {
            data = m.writeValueAsString(dto);
        } catch (Exception e) {
            return Response.status(402).entity(e.getMessage()).build();
        }

        return Response.status(200).entity(data).build();

    }

}
