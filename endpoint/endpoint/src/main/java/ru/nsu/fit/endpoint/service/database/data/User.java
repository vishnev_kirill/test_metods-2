package ru.nsu.fit.endpoint.service.database.data;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ru.nsu.fit.endpoint.Dto.CustomerDto;
import ru.nsu.fit.endpoint.Dto.UserDto;
import ru.nsu.fit.endpoint.Exceptions.*;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
@javax.persistence.Entity
@Table(schema = "public")
public class User extends MyEntity {
    public static final String INCORRECT_LEN_Of_FIRSTNAME="FirstName length should be more or equal 2 symbols and less or equal 12 symbols";
    public static final String INCORRECT_FORMAT_Of_FIRSTNAME="Incorrect format of firstName";
    public static final String NULL_FIRSTNAME="Not a firstName";
    public static final String INCORRECT_LEN_Of_LASTNAME="LastName length should be more or equal 2 symbols and less or equal 12 symbols";
    public static final String INCORRECT_FORMAT_Of_LASTNAME="Incorrect format of lastName";
    public static final String NULL_LASTNAME="Not a lastName";

    public static final String INCORRECT_LOGIN ="incorrect format of login";
    public static final String NULL_LOGIN="Not a login";

    public static final String INCORRECT_LEN_Of_PASSWORD = "Password's length should be more or equal 6 symbols and less or equal 12 symbols";
    public static final String INCORRECT_PASSWORD ="incorrect format of password";
    public static final String NULL_PASSWORD="Not a password";



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String firstName;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String lastName;
    /* указывается в виде email, проверить email на корректность */
    @Column(unique = true)
    private String login;
    /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
    private String pass;
    private UserRole userRole;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public static enum UserRole {
        COMPANY_ADMINISTRATOR("Company administrator"),
        TECHNICAL_ADMINISTRATOR("Technical administrator"),
        BILLING_ADMINISTRATOR("Billing administrator"),
        USER("User");

        private String roleName;

        UserRole(String roleName) {
            this.roleName = roleName;
        }

        public String getRoleName() {
            return roleName;
        }

        public static UserRole getRole(String roleName)
        {
            switch (roleName){
                case "User":
                    return USER;
                case "Billing administrator":
                    return BILLING_ADMINISTRATOR;
                case "Technical administrator":
                    return TECHNICAL_ADMINISTRATOR;
                case "Company administrator":
                   return COMPANY_ADMINISTRATOR;
            }
            return USER;
        }
    }


    public User(String firstName, String lastName, String login, String pass) throws MyExcepton {
        validate(firstName, lastName, login, pass);
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.pass = pass;
    }

    public User(){

    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_subscription",
    joinColumns = @JoinColumn(name = "user_id"),
    inverseJoinColumns = @JoinColumn(name = "subscription_id"))
    @Fetch(value = FetchMode.SUBSELECT)
    private Collection<Subscription> subscriptions= new HashSet<Subscription>();

    public Collection<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(Collection<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    private static void validate(String firstName, String lastName, String login, String pass) throws MyExcepton {
        validateFirstName(firstName);
        validateLastName(lastName);
        validateLogin(login);
        validatePasssword(pass,firstName,lastName, login);
    }
    private static void validateFirstName(String firstName) throws MyExcepton{
        if(firstName!=null){
            if(!(firstName.length()>=2 && firstName.length()<13))
                throw new InvalidUserNameException(INCORRECT_LEN_Of_FIRSTNAME);
            if(!firstName.matches("[A-Z][a-z]*")){
                throw new InvalidUserNameException(INCORRECT_FORMAT_Of_FIRSTNAME);
            }
        }else
            throw new InvalidUserNameException(NULL_FIRSTNAME);

    }

    private static void validateLastName(String lastName) throws MyExcepton{

        if(lastName!=null){
            if(!(lastName.length()>=2 && lastName.length()<13)) {
                throw new InvalidUserLastNameException(INCORRECT_LEN_Of_LASTNAME);
            }
            if(!lastName.matches("[A-Z][a-z]*")){
                throw new InvalidUserLastNameException(INCORRECT_FORMAT_Of_LASTNAME);
            }
        }else
            throw new InvalidUserLastNameException(NULL_LASTNAME);

    }
    private static void validateLogin(String login) throws MyExcepton{
        if(login != null){
            if(!login.contains("@")){//regex
                throw new InvalidLoginException(INCORRECT_LOGIN);
            }
        } else
            throw new InvalidLoginException(NULL_LOGIN);

    }
    private static void validatePasssword(String pass,String firstName, String lastName, String login) throws MyExcepton{
        if(pass!=null){
            if(!(pass.length()>=6 && pass.length()<13))
                throw new InvalidPasswordException(INCORRECT_LEN_Of_PASSWORD);
            if(pass.contains(firstName) || pass.contains(lastName) || pass.contains(login)){
                throw new InvalidPasswordException(INCORRECT_PASSWORD);
            }
        }else
            throw new InvalidPasswordException(NULL_PASSWORD);


    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) throws MyExcepton {
        validateFirstName(firstName);
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) throws MyExcepton {
        validateLastName(lastName);
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) throws MyExcepton {
        validateLogin(login);
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) throws MyExcepton {

        validatePasssword(this.getPass(),this.getFirstName(),this.getLastName(),this.getLogin());
        this.pass = pass;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }


    public void setData(UserDto dto) throws MyExcepton{
        validate(dto.getFirstname(),dto.getLastName(),dto.getLogin(), dto.getPass());
        this.firstName=dto.getFirstname();
        this.lastName=dto.getLastName();
        this.login=dto.getLogin();
        this.pass=dto.getPass();
        this.userRole= UserRole.getRole(dto.getRole());


    }

    public UserDto GetData()  throws  MyExcepton{
        UserDto dto= new UserDto();
        dto.setId(this.id);
        dto.setFirstname(this.firstName);
        dto.setLastName(this.lastName);
        dto.setLogin(this.login);
        dto.setPass("***");
        dto.setRole(this.userRole.getRoleName());
        dto.setCustomeId(this.customer.getId());
        return dto;
    }
}
