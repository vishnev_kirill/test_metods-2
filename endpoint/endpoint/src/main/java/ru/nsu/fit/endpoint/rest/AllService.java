package ru.nsu.fit.endpoint.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.nsu.fit.endpoint.Dto.CustomerDto;
import ru.nsu.fit.endpoint.Dto.PlanDto;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.DataService;
import ru.nsu.fit.endpoint.service.database.data.ServicePlan;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kirill on 14.11.2016.
 */

@Path("/all")
public class AllService implements Serializable {

    @Inject
    DataService service;


    @GET
    @Path("/getPlans")
    @Consumes("application/json")
    public Response getMsg() {
        List<ServicePlan> cust=service.getAllPlans();
        if(cust.size()==0)
            return Response.status(200).entity("No plans in the system").build();

        ObjectMapper m= new ObjectMapper();
        List<PlanDto> dto= new ArrayList<PlanDto>();
        for(ServicePlan c  : cust){
            dto.add(c.getData());

        }

        String data=null;
        try {
            data = m.writeValueAsString(dto);
        }catch (Exception e){
            return Response.status(402).entity(e.getMessage()).build();
        }
        return Response.status(200).entity(data).build();

    }
}