package ru.nsu.fit.endpoint.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.nsu.fit.endpoint.Dto.*;
import ru.nsu.fit.endpoint.Exceptions.MyExcepton;
import ru.nsu.fit.endpoint.mock.PlanRegistrator;
import ru.nsu.fit.endpoint.service.database.data.*;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by kirill on 12.11.2016.
 */
@Path("/customer")
@DeclareRoles({"ADMIN", "CUSTOMER", "USER"})
public class CustomerService implements Serializable {

    @Inject
    DataService service;


    @POST
    @RolesAllowed("CUSTOMER")
    @Path("/getId")
    @Produces("application/json")
    public Response getIdByLogin(String login) {
        List<Customer> cust = service.getAllCustomer();
        int id = 0;
        for (Customer c : cust) {
            if (c.getLogin().equals(login))
                id = c.getId();
        }

        if (id == 0)
            return Response.status(402).entity("No Customer in system").build();

        return Response.status(200).entity(id).build();

    }


    @POST
    @RolesAllowed("CUSTOMER")
    @Path("/getMyData")
    @Produces("application/json")
    public Response getMyData(String i) {
        int  id=Integer.parseInt(i);
        Customer customer = service.getEntity(Customer.class, id);
        if (customer == null)
            return Response.status(401).entity("Not Customer").build();
        CustomerDto dto = customer.getData();
        ObjectMapper m = new ObjectMapper();
        String data = null;
        try {
            data = m.writeValueAsString(dto);
        } catch (Exception e) {
            return Response.status(402).entity(e.getMessage()).build();
        }

        return Response.status(200).entity(data).build();

    }

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/addUser")
    @Consumes("application/json")
    public Response addUser(String data) {
        User u = new User();
        ObjectMapper m = new ObjectMapper();
        UserDto dto = null;
        try {
            dto = m.readValue(data, UserDto.class);
        } catch (Exception e) {
            return Response.status(402).entity(e.getMessage()).build();
        }
        Customer c = service.getEntity(Customer.class, dto.getCustomeId());
        if (c == null)
            return Response.status(401).entity("Not Customer").build();
        String arr = null;
        try {
            u.setData(dto);

        } catch (MyExcepton e) {
            return Response.status(402).entity(e.getMessage()).build();
        }
        u.setCustomer(c);
        try {
            service.Add(u);
        }catch (Exception e){
            return Response.status(402).entity("User have been added already").build();
        }
        return Response.status(200).entity("OK").build();
    }

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/getMyUsers")
    @Consumes("application/json")
    public Response getMyUsers(String i) {
        int id= Integer.parseInt(i);
        Customer customer = service.getEntity(Customer.class, id);
        if (customer == null)
            return Response.status(401).entity("Not Customer").build();
        Collection<User> users = customer.getUsers();


        ObjectMapper m = new ObjectMapper();
        List<UserDto> dto = new ArrayList<UserDto>();
        for (User c : users) {
            dto.add(c.GetData());

        }

        String data = null;
        try {
            data = m.writeValueAsString(dto);
        } catch (Exception e) {
            return Response.status(402).entity(e.getMessage()).build();
        }
        return Response.status(200).entity(data).build();

    }

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/changeUserRole")
    @Consumes("application/json")
    public Response changeUserRole(String data) {
        ObjectMapper m = new ObjectMapper();
        ChangeRoleDto dto = null;
        try {
            dto = m.readValue(data, ChangeRoleDto.class);
        } catch (Exception e) {
            return Response.status(402).entity(e.getMessage()).build();
        }
        Customer c = service.getEntity(Customer.class, dto.getCustomerId());
        if (c == null)
            return Response.status(401).entity("Not Customer").build();

        Collection<User> users = c.getUsers();
        for (User user : users) {
            if(dto.getLogin().equals(user.getLogin()))
            {
                user.setUserRole(User.UserRole.getRole(dto.getNewRole()));
                service.save(user);
            }
        }
        return Response.status(200).entity("OK").build();
    }


    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/getMySubscriptions")
    @Consumes("application/json")
    public Response getMySubscriptions(String i) {
        int id=Integer.parseInt(i);
        Customer customer = service.getEntity(Customer.class, id);
        if (customer == null)
            return Response.status(401).entity("Not Customer").build();
        Collection<Subscription> subscriptions = customer.getSubscriptions();


        ObjectMapper m = new ObjectMapper();
        List<SubscriptionDto> dto = new ArrayList<SubscriptionDto>();
        for (Subscription c : subscriptions) {
            dto.add(c.getData());

        }

        String data = null;
        try {
            data = m.writeValueAsString(dto);
        } catch (Exception e) {
            return Response.status(402).entity(e.getMessage()).build();
        }
        return Response.status(200).entity(data).build();

    }

    @POST
    @RolesAllowed("CUSTOMER")
    @Path("/dellUser")
    @Consumes("application/json")
    public Response dellUser(String data) {
        int id=Integer.parseInt(data);
        User cust=service.getEntity(User.class,id);
        service.remove(cust);
        return Response.status(200).entity("OK").build();

    }

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/addMoney")
    @Consumes("application/json")
    public Response addMoney(String data) {
        ObjectMapper m = new ObjectMapper();
        DepositDto dto = null;
        try {
            dto = m.readValue(data, DepositDto.class);
        } catch (Exception e) {
            return Response.status(402).entity(e.getMessage()).build();
        }
        Customer c = service.getEntity(Customer.class, dto.getId());
        if (c == null)
            return Response.status(401).entity("Not Customer").build();

        if (dto.getMoney()<0)
             return Response.status(402).entity("money<0").build();
        c.setMoney(c.getMoney()+dto.getMoney());
        service.save(c);
        return Response.status(200).entity("OK").build();
    }



    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/BuyPlan")
    @Consumes("application/json")
    public Response ByPlan(String data) {
        ObjectMapper m = new ObjectMapper();
        BuyDto dto = null;
        try {
            dto = m.readValue(data, BuyDto.class);
        } catch (Exception e) {
            return Response.status(402).entity(e.getMessage()).build();
        }

         String mes=PlanRegistrator.register(dto,service);

         return Response.status(200).entity(mes).build();
    }


    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/addUserInSubs")
    @Consumes("application/json")
    public Response addUserInSubs(String data) {
        ObjectMapper m = new ObjectMapper();
        UserSubscriptionDto dto = null;
        try {
            dto = m.readValue(data, UserSubscriptionDto.class);
        } catch (Exception e) {
            return Response.status(402).entity(e.getMessage()).build();
        }

        Customer cust = service.getEntity(Customer.class, dto.getCustomerId());
        if (cust == null)
            return Response.status(401).entity("Not Customer").build();

        Collection<User>  users = cust.getUsers();
        User u=null;
        for(User us : users){
            if(us.getCustomer().getId()==dto.getCustomerId())
                u=us;
        }

         if (u == null)
            return Response.status(401).entity("Not User").build();

        Subscription subs = service.getEntity(Subscription.class, dto.getSubscriptionId());
        if (subs == null)
            return Response.status(401).entity("Not Customer").build();

        ServicePlan p=subs.getServicePlan();
        int money =cust.getMoney();
        if(money-p.getFeePerUnit()<0) {
            return Response.status(402).entity("You don't have enought money").build();
        }
        cust.setMoney(money-p.getFeePerUnit());
        subs.setUsedSeats(subs.getUsedSeats()+1);
        u.getSubscriptions().add(subs);
        service.save(cust);
        service.save(subs);
        service.save(u);
        return Response.status(200).entity("Plan added").build();
    }


    @POST
    @RolesAllowed("CUSTOMER")
    @Path("/dellSubscription")
    @Consumes("application/json")
    public Response dellSubscription(String data) {
        int id=Integer.parseInt(data);
        Subscription subs=service.getEntity(Subscription.class,id);
        service.remove(subs);
        return Response.status(200).entity("OK").build();

    }


}
