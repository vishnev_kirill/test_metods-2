package ru.nsu.fit.endpoint.service.database.data;

import ru.nsu.fit.endpoint.Dto.ChangeRoleDto;
import ru.nsu.fit.endpoint.Dto.UserDto;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.MyEntity;
import ru.nsu.fit.endpoint.service.database.data.ServicePlan;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.List;

/**
 * Created by kirill on 12.11.2016.
 */
@Stateless
public class DataService {

    @PersistenceContext(unitName = "datas")
    private EntityManager manager;

    public String Add(MyEntity c) {
        manager.persist(c);

        return "Success";
    }

    public <T> T getEntity(Class<T> cls, int id) {
        T res = manager.find(cls, id);
        return res;
    }

    public Integer GetCustomerIdByLogin(String login) {
        return manager.createQuery("SELECT c.id FROM Customer c where c.login like:log")
                .setParameter("log", login)
                .getFirstResult();
    }


    public Integer GetUserIdByLogin(String login) {
        return manager.createQuery("SELECT c.id FROM User c where c.login like :lg")
                .setParameter("lg", login)
                .getFirstResult();
    }


    public List<ServicePlan> getAllPlans() {
        return manager.createQuery("from ServicePlan ", ServicePlan.class).getResultList();
    }

    public List<Customer> getAllCustomer() {
        return manager.createQuery("from Customer ", Customer.class).getResultList();
    }


    public List<User> getAllUsers() {
        return manager.createQuery("from User ", User.class).getResultList();
    }

    public String getRole(String log, String pass) {
        List<String> s = manager.createQuery("SELECT c.pass FROM Customer c where c.login like :lg")
                .setParameter("lg", log)
                .getResultList();
        for (String st : s) {
            if (pass.equals(st))
                return "CUSTOMER";


        }

        List<String> su = manager.createQuery("SELECT c.pass FROM User c where c.login like :lg")
                .setParameter("lg", log)
                .getResultList();
        for (String st : su) {
            if (pass.equals(st))
                return "USER";


        }
        return "ALL";


    }

    public void save(MyEntity e){

        manager.merge(e);

    }

    public void remove(MyEntity e){

        manager.remove(manager.merge(e));

    }

   public Collection<User> getUsersByCustomerId(int id){
       return manager.createQuery("SELECT c.users from Customer c where c.id=id").getResultList();
   }
}



