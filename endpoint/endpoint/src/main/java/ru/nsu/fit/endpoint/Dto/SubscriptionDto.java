package ru.nsu.fit.endpoint.Dto;

/**
 * Created by kirill on 12.11.2016.
 */
public class SubscriptionDto {
    int id;
    String name;
    String customer;
    String status;
    int usedSeats;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public int getUsedSeats() {
        return usedSeats;
    }

    public void setUsedSeats(int usedSeats) {
        this.usedSeats = usedSeats;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString(){
        return new StringBuffer(" Name: ").append(this.getName()).
                append(" Id ").append(this.getId()).
                append(" Customer ").append(this.getCustomer()). append(" used ").append(this.getUsedSeats()).toString();
    }
}
