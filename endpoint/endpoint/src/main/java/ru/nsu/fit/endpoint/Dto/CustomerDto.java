package ru.nsu.fit.endpoint.Dto;

/**
 * Created by kirill on 12.11.2016.
 */
public class CustomerDto {

    int id;
    String firstname;
    String lastName;
    String Login;
    String pass;
    int money;

    public CustomerDto(){

    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String login) {
        Login = login;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString(){
        return new StringBuffer("firstname: ").append(this.getFirstname()).
                append("LastName ").append(this.getLastName()).
                append("Login  ").append(this.getLogin()).append(" money").append(this.getMoney()).toString();
    }
}
