package ru.nsu.fit.endpoint.rest;


import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by kirill on 01.11.2016.
 */
@ApplicationPath("/rest")
public class JaxRsActvator extends Application {
    //private Set<Object> singletons= new HashSet<Object>();
    private HashSet<Class<?>> set = new HashSet<Class<?>>();
    public JaxRsActvator(){
         set.add(SecurityBean.class);
         set.add(AdminService.class);
         set.add(CustomerService.class);
         set.add(UserService.class);
         set.add(AllService.class);
    }
    @Override
    public Set<Class<?>>  getClasses() {
        return set;
    }
    // /@Override
    //public Set<Object> getSingletons() {
      //  return singletons;
    //}
}

