package ru.nsu.fit.endpoint.mock;

import ru.nsu.fit.endpoint.Dto.BuyDto;
import ru.nsu.fit.endpoint.service.database.data.DataService;
import ru.nsu.fit.endpoint.service.database.data.ServicePlan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;

import java.util.Collection;
import java.util.List;

/**
 * Created by kirill on 19.11.2016.
 */
public class Mock implements Runnable{

    DataService service;
    Subscription subckription;
    ServicePlan plan;
    Collection<Subscription> subckriptions;

    public Mock(DataService service, Subscription subck, Collection<Subscription> l, ServicePlan plan){
             this.service=service;
             this.subckription=subck;
             this.subckriptions=l;
             this.plan=plan;
    }

    public void run() {
        try {
            this.wait(200);
            for(Subscription sub : subckriptions){
                int max=sub.getServicePlan().getMaxSeats();
                if(max==plan.getMaxSeats()) {
                   service.remove(subckription);
                    return;
                }
            }


            subckription.setStatus(Subscription.Status.DONE);
            //service.Add(subckription);
        }catch (InterruptedException ex){
            ex.printStackTrace();

        }

    }
}

