package ru.nsu.fit.endpoint.rest;



import com.fasterxml.jackson.databind.ObjectMapper;
import ru.nsu.fit.endpoint.Dto.CustomerDto;
import ru.nsu.fit.endpoint.Dto.PlanDto;
import ru.nsu.fit.endpoint.Dto.UserDto;
import ru.nsu.fit.endpoint.Exceptions.MyExcepton;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.DataService;
import ru.nsu.fit.endpoint.service.database.data.ServicePlan;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */

@Path("/admin")
@DeclareRoles({"ADMIN", "CUSTOMER","USER"})
public class AdminService implements Serializable {

    @Inject
    DataService service;

    @POST
    @RolesAllowed("ADMIN")
    @Path("/addCustomer")
    @Consumes("application/json")
    public Response addCustomer(String data) {
        Customer c= new Customer();
        ObjectMapper m = new ObjectMapper();
        CustomerDto dto=null;
        try {
            dto = m.readValue(data, CustomerDto.class);
        }catch (Exception e){
            return Response.status(402).entity(e.getMessage()).build();
        }
        String arr=null;
        try {
            c.setData(dto);
        }catch (MyExcepton e){
            return Response.status(402).entity(e.getMessage()).build();
        }
        try {
            service.Add(c);
        }catch (Exception e){
            return Response.status(402).entity("Customer have been added already").build();
        }
        return Response.status(200).entity("OK").build();

    }

    @GET
    @RolesAllowed("ADMIN")
    @Path("/getCustomers")
    @Produces("application/json")
    public Response getCustomers() {
        List<Customer> cust=service.getAllCustomer();
        if(cust.size()==0)
            return Response.status(200).entity("No Customer in system").build();

        ObjectMapper m= new ObjectMapper();
        List<CustomerDto> dto= new ArrayList<CustomerDto>();
        for(Customer c  : cust){
            dto.add(c.getData());

        }
        String data=null;
        try {
             data = m.writeValueAsString(dto);
        }catch (Exception e){
            return Response.status(402).entity(e.getMessage()).build();
        }
        return Response.status(200).entity(data).build();
    }

    @POST
    @RolesAllowed("ADMIN")
    @Path("/addPlan")
    @Consumes("application/json")
    public Response addPlan(String data) {
        ServicePlan c= new ServicePlan();
        ObjectMapper m = new ObjectMapper();
        PlanDto dto=null;
        try {
            dto = m.readValue(data, PlanDto.class);
        }catch (Exception e){
            return Response.status(402).entity(e.getMessage()).build();
        }
        String arr=null;
        try {
            c.setData(dto);
        }catch (MyExcepton e){
            return Response.status(402).entity(e.getMessage()).build();
        }
        try {
            service.Add(c);
        } catch (Exception e){
            return Response.status(402).entity("Plan have been added already").build();
        }
        return Response.status(200).entity("OK").build();

    }



    @POST
    @RolesAllowed("ADMIN")
    @Path("/dellCustomer")
    @Consumes("application/json")
    public Response dellCustomer(String data) {
        int id=Integer.parseInt(data);
        Customer cust=service.getEntity(Customer.class,id);
        service.remove(cust);
        return Response.status(200).entity("OK").build();

    }


    @POST
    @RolesAllowed("ADMIN")
    @Path("/dellPlan")
    @Consumes("application/json")
    public Response dellPlan(String data) {
        Collection<ServicePlan> plans=service.getAllPlans();
        for(ServicePlan plan : plans){
            if(plan.getName().equals(data)){
//                service.save(plan);
                service.remove(plan);
                return Response.status(200).entity("OK").build();
            }
        }

        return Response.status(402).entity("not plan").build();

    }
}