package ru.nsu.fit.endpoint.service.database.data;

import ru.nsu.fit.endpoint.Dto.SubscriptionDto;
import ru.nsu.fit.endpoint.Exceptions.MyExcepton;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */

@Entity
public class Subscription extends MyEntity {

    public static final String SMAL_MAXSEATS = "max seats  should not be less then 1";
    public static final String BIG_MAXSEATS = "max seats  should not be more  999999";
    public static final String SMAL_MINSEATS = "min seats  should not be less then 1";
    public static final String BIG_MINSEATS = "min seats  should not be more then 999999";
    public static final String NOT_CORRECT_SEATS = "maxseats < minseats";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    public int getUsedSeats() {
        return usedSeats;
    }

    public void setUsedSeats(int usedSeats) throws MyExcepton {
        int max = servicePlan.getMaxSeats();
        if ( usedSeats <= max) {
            this.usedSeats = usedSeats;
            return;
        }
        throw new MyExcepton("Not valid usedSeats");

    }

    public Collection<User> getUsers() {
        return users;
    }

    public void setUsers(Collection<User> users) {
        this.users = users;
    }

    private int usedSeats;
    private Status status;

    public Subscription(int usedseats) {
        this.usedSeats = usedseats;


    }

    public Subscription() {

    }

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer mycustomer;

    public Customer getCustomer() {
        return mycustomer;
    }

    public void setCustomer(Customer customer) {
        this.mycustomer = customer;
    }


    @ManyToOne
    @JoinColumn(name = "serviceplan_id")
    private ServicePlan servicePlan;

    public ServicePlan getServicePlan() {
        return servicePlan;
    }

    public void setServicePlan(ServicePlan servicePlan) {
        this.servicePlan = servicePlan;
    }

    public static enum Status {
        PROVISION("provision"),
        DONE("done");

        private String status;

        Status(String roleName) {
            this.status = roleName;
        }

        public String getStatus() {
            return status;
        }
    }


    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @ManyToMany
    @JoinTable(name = "user_subscription",
            joinColumns = @JoinColumn(name = "subscription_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Collection<User> users = new HashSet<User>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SubscriptionDto getData() {
        SubscriptionDto dto = new SubscriptionDto();
        dto.setId(this.getId());
        dto.setName(this.servicePlan.getName());
        dto.setCustomer(this.mycustomer.getLogin());
        dto.setUsedSeats(this.usedSeats);
        dto.setStatus(this.status.getStatus());
        return dto;
    }

}


