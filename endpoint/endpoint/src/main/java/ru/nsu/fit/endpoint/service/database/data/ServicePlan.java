package ru.nsu.fit.endpoint.service.database.data;

import ru.nsu.fit.endpoint.Dto.PlanDto;
import ru.nsu.fit.endpoint.Dto.UserDto;
import ru.nsu.fit.endpoint.Exceptions.*;

import javax.persistence.*;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
@Entity
public class ServicePlan extends MyEntity {

    public static final String UNCORECT_LEN_Of_NAME="Name length should be more or equal 2 symbols and less or equal 128 symbols";
    public static final String UNCORECT_FORMAT_Of_NAME="Uncorrect format of Name";
    public static final String NULL_NAME="Not a name";
    public static final String UNCORECT_LEN_Of_DETAILS="details length should be more or equal 1 symbols and less or equal 1025 symbols";
    public static final String NULL_DETAILS="Not a details";
    public static final String SMAL_MAXSEATS="max seats  should not be less then 1";
    public static final String BIG_MAXSEATS="max seats  should not be more  999999";
    public static final String SMAL_MINSEATS="min seats  should not be less then 1";
    public static final String BIG_MINSEATS="min seats  should not be more then 999999";
    public static final String NOT_CORRECT_SEATS="maxseats < minseats";
    public static final String BIG_FREEPERUNIT="freePerUnit  should not be less then 1000000";
    public static final String SMAL_FREEPERUNIT="freePerUnit  should not be less then 0";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "serviceplan_id")
    private int id;
    /* Длина не больше 128 символов и не меньше 2 включительно не содержит спец символов */
    @Column(unique = true)
    private String name;
    /* Длина не больше 1024 символов и не меньше 1 включительно */
    private String details;
    /* Не больше 999999 и не меньше 1 включительно */
    private int maxSeats;
    /* Больше ли равно 0 но меньше либо равно 999999 */
    private int feePerUnit;


    public  ServicePlan( String name, String details, int maxSeats, int minSeats, int freePerUnit) throws MyExcepton{
        validate(name, details, maxSeats,freePerUnit);
        this.name = name;
        this.details = details;
        this.maxSeats = maxSeats;
        this.feePerUnit = freePerUnit;
    }

    public ServicePlan(){

    }



    private static void validate(String name, String details, int maxSeats,  int freePerUnit) throws MyExcepton{
                  validateName(name);
                  validateDetails(details);
                  validateSeats(maxSeats);
                  validatefreePerUnit(freePerUnit);
            }

    private static void validateName(String name) throws MyExcepton{
        if(name!=null){
            if(!(name.length()>=2 && name.length()<129))
                throw new InvalidServiceNameException(UNCORECT_LEN_Of_NAME);
            //if(!name.matches("[A-Z][a-z]*")){
              //  throw new InvalidServiceNameException(UNCORECT_FORMAT_Of_NAME);
            //}
        }else
            throw new InvalidServiceNameException(NULL_NAME);


    }
    private static void validateDetails(String details) throws MyExcepton{
        if(details!=null){
            if(!(details.length()>=1 && details.length()<1025)) {
                throw new InvalidDetailsException(UNCORECT_LEN_Of_DETAILS);
            }
        }else
            throw new InvalidDetailsException(NULL_DETAILS);

    }
    private static void validateSeats(int maxSeats) throws MyExcepton{
        if (maxSeats<1)
            throw new SeatsException(SMAL_MAXSEATS);

        if (maxSeats>=1000000)
            throw new SeatsException(BIG_MAXSEATS);


    }
    private static void validatefreePerUnit(int freePerUnit) throws MyExcepton{
        if (freePerUnit<0)
            throw new InvalidFreePerUnitException(SMAL_FREEPERUNIT);


        if (freePerUnit>=1000000)
            throw new InvalidFreePerUnitException(BIG_FREEPERUNIT);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws MyExcepton {
        validateName(name);
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) throws MyExcepton {
        validateDetails(details);
        this.details = details;
    }

    public int getMaxSeats() {
        return maxSeats;
    }

    public void setMaxSeats(int maxSeats) {
        this.maxSeats = maxSeats;
    }


    public int getFeePerUnit() {
        return feePerUnit;
    }

    public void setFeePerUnit(int feePerUnit) throws MyExcepton {
        validatefreePerUnit(feePerUnit);
        this.feePerUnit = feePerUnit;
    }

    public void SetSeats(int maxSeats,int minSeats) throws MyExcepton{
        validateSeats(maxSeats);
        this.setMaxSeats(maxSeats);

    }


    public void setData(PlanDto dto) throws MyExcepton{
        validate(dto.getName(),dto.getDetails(),dto.getMaxSeats(), dto.getFreePerUnit());
        this.name=dto.getName();
        this.details=dto.getDetails();
        this.maxSeats=dto.getMaxSeats();
        this.feePerUnit= dto.getFreePerUnit();


    }

    public PlanDto getData()  throws  MyExcepton{
        PlanDto dto= new PlanDto();
        dto.setId(this.id);
        dto.setName(this.name);
        dto.setDetails(this.details);
        dto.setMaxSeats(this.maxSeats);
        dto.setFreePerUnit(this.feePerUnit);
        return dto;
    }
}


