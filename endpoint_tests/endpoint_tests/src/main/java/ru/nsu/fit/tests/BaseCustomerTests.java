package ru.nsu.fit.tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.utils.AllureUtils;
import ru.nsu.fit.utilsfortesting.ProviderService;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import java.io.IOException;

/**
 * Created by kirill on 14.11.2016.
 */

@Title("BaseCustomerTests")
public class BaseCustomerTests extends CreateCustomerTests {

    protected int customer_id=0;

    @BeforeClass
    @Step("Authorisation")
    @Title("get Customer id")
    @Description("Get customer id via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Authentication feature")
    public void getCustomerId() throws IOException {
        WebTarget webTarget = ProviderService.getTarget(customerDto.getLogin(),customerDto.getPass(),"customer/getId");
        String s = ProviderService.postString(customerDto.getLogin(),webTarget);
        System.out.println("авторизация customer");
        customer_id = Integer.parseInt(s);
        AllureUtils.saveTextLog("id " + s);

    }



}
