package ru.nsu.fit.tests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.*;
import ru.nsu.fit.dto.BuyDto;
import ru.nsu.fit.dto.PlanDto;
import ru.nsu.fit.dto.SubscriptionDto;
import ru.nsu.fit.utils.AllureUtils;
import ru.nsu.fit.utilsfortesting.DataGenerator;
import ru.nsu.fit.utilsfortesting.ProviderService;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import java.io.IOException;
import java.util.List;

/**
 * Created by kirill on 21.11.2016.
 */

@Title("SubscriptionTest")
public class SubscriptionTest extends CreateUserTest {

    protected List<PlanDto> plans;
    protected PlanDto plan;
    protected SubscriptionDto subscription;


    @BeforeClass
    @Step("add plan in base")
    public void createPlans() throws IOException {

        ProviderService.createTestGroupPlans();
        plans=ProviderService.getPlans();
        for(PlanDto p : plans){
            if(p.getName().equals(ProviderService.PLAN_NAME1))
                plan=p;
        }

        WebTarget webTarget = ProviderService.getTarget(customerDto.getLogin(), customerDto.getPass(), "customer/BuyPlan");
        BuyDto dto = DataGenerator.getBuyDto(customer_id, plan.getId());
        String req = ProviderService.postDto(dto, webTarget);
        AllureUtils.saveTextLog("Status " + req);
        System.out.println("Подписались на план");
        Assert.assertEquals(req, "Plan added");
    }



    @Test
    @Step("Check Plan")
    @Description("check paln via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("plan feature")
    public void checkPlan() throws IOException {
        WebTarget webTarget = ProviderService.getTarget(customerDto.getLogin(), customerDto.getPass(), "customer/getMySubscriptions");
        String req = ProviderService.postString(Integer.toString(customer_id), webTarget);
        System.out.println("Проверка планa");
        ObjectMapper m = new ObjectMapper();
        List<SubscriptionDto> list = m.readValue(req, new TypeReference<List<SubscriptionDto>>(){});
        for (SubscriptionDto d : list) {
            if (d.getName().equals(plan.getName())) {
                subscription=d;
                Assert.assertEquals(d.getName().equals(plan.getName()), true);
            }

        }


    }

    @Test                                 // Возможно, это поведение лишнее.
    @Step("Buy Plan again")
    @Description("check buy plan again via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("plan feature")
    public void buyPlanAgain() throws IOException {
        WebTarget webTarget = ProviderService.getTarget(customerDto.getLogin(), customerDto.getPass(), "customer/BuyPlan");
        BuyDto dto = DataGenerator.getBuyDto(customer_id, plan.getId());
        String req = ProviderService.postDto(dto, webTarget);
        AllureUtils.saveTextLog("server say" + req);
        Assert.assertEquals(req, "you subscriptions not compatible");


    }

    @AfterClass
    @Step("Dell subscription")
    public void dellSubscription() throws IOException{
        WebTarget webTarget = ProviderService.getTarget(customerDto.getLogin(), customerDto.getPass(), "customer/dellSubscription");
        String str = ProviderService.postString(Integer.toString(subscription.getId()),webTarget);
        AllureUtils.saveTextLog("Status: " + str);
        Assert.assertEquals(str, "OK");
        ProviderService.dellPlans(plans);
    }


}