package ru.nsu.fit.tests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.dto.ChangeRoleDto;
import ru.nsu.fit.dto.UserDto;
import ru.nsu.fit.utils.AllureUtils;
import ru.nsu.fit.utilsfortesting.DataGenerator;
import ru.nsu.fit.utilsfortesting.ProviderService;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import java.io.IOException;
import java.util.List;

/**
 * Created by Алия on 18.11.2016.
 */

@Title("Change User Role")
public class ChangeRoleTest extends CreateUserTest {

    public String newRole = "Billing administrator";

    @Test(dependsOnMethods = {"checkUser"})
    @Title("Change user Role")
    @Step("Change user Role")
    @Description("chnage role")
    @Severity(SeverityLevel.MINOR)
    @Features("Change role feature")
    public void changeUserRole() throws IOException {
        WebTarget webTarget = ProviderService.getTarget(customerDto.getLogin(),customerDto.getPass(),"customer/changeUserRole");
        ChangeRoleDto dto = DataGenerator.getChangeRoleDto(customer_id,user.getLogin(),newRole);
        String s = ProviderService.postDto(dto,webTarget);
        System.out.println("Поменяли роль " +s);
        Assert.assertEquals("OK", s);
        AllureUtils.saveTextLog("Status " + s + " change on role " + newRole);
    }

    @Test(dependsOnMethods = {"changeUserRole"})
    @Title("Check data user")
    @Step("Check data user")
    @Description("Check role")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Authentication feature")
    public void checkChangeUser() throws IOException {
        WebTarget webTarget = ProviderService.getTarget(customerDto.getLogin(),customerDto.getPass(),"customer/getMyUsers");
        String users = ProviderService.postString(Integer.toString(customer_id),webTarget);
        ObjectMapper m = new ObjectMapper();
        List<UserDto> list = m.readValue(users, new TypeReference<List<UserDto>>() {});

        UserDto userDto = new UserDto();
        for (UserDto d : list){
            if(d.getLogin().equals(user.getLogin())){
                userDto = d;
            }
        }
        System.out.println(userDto.getRole());
        System.out.println(newRole);
        boolean flag = userDto.getRole().equals(newRole);
        Assert.assertEquals(true,flag);
        System.out.println(flag);
        System.out.println("Проверили роль" + flag);
        AllureUtils.saveTextLog("new role " + userDto.getRole());
    }

}
