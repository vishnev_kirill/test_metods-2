package ru.nsu.fit.utilsfortesting;

import io.codearte.jfairy.Fairy;
import io.codearte.jfairy.producer.person.Person;
import ru.nsu.fit.dto.*;

/**
 * Created by kirill on 22.11.2016.
 */
public class DataGenerator {
   // Алие
   // Разобраться с jfairy, все поля генерировать.

    public static Fairy fairy = Fairy.create();
    public static Person person = fairy.person();
    public static Person user = fairy.person();

    public static String cname = person.firstName();
    public static String clastName = person.lastName();
    public static String clogin = person.email();
    public static String cpassword = person.password();


    public static String uname = user.firstName();
    public static String ulastName = user.lastName();
    public static String ulogin = user.email();
    public static String upassword = user.password();



    public static String name = fairy.textProducer().latinWord();
    public static String details = fairy.textProducer().sentence(20);
    public static int maxSeats = 10;
    public static int freePerUnit = 10;






    public static UserDto getUserDto() {
        UserDto dto = new UserDto();
        dto.setFirstname(uname);
        dto.setLastName(ulastName);
        dto.setLogin(ulogin);
        dto.setPass(upassword);
        dto.setRole("User");  // По умоолчанию
        dto.setCustomeId(0); // всегда, выше вставим
        return dto;
    }

    public static CustomerDto getCustomerDto() {
        CustomerDto dto = new CustomerDto();
        dto.setFirstname(cname);
        dto.setLastName(clastName);
        dto.setLogin(clogin);
        dto.setPass(cpassword);
        dto.setMoney(100);
        return dto;
    }

    public static PlanDto getPlanDto() {
        PlanDto dto = new PlanDto();
        dto.setId(0);
        dto.setName(name);
        dto.setDetails(details);
        dto.setMaxSeats(maxSeats);
        dto.setFreePerUnit(freePerUnit);
        return dto;
    }

    public static ChangeRoleDto getChangeRoleDto(int customerId, String login, String newRole) {
        ChangeRoleDto dto = new ChangeRoleDto();
        dto.setCustomerId(customerId);
        dto.setLogin(login);
        dto.setNewRole(newRole);
        return dto;
    }


    public static BuyDto getBuyDto(int customerId, int planId) {
        BuyDto dto = new BuyDto();
        dto.setCustomerId(customerId);
        dto.setPlanId(planId);
        return dto;
    }

    public static DepositDto getDepositDto(int id, int money) {
        DepositDto dto = new DepositDto();
        dto.setMoney(money);
        dto.setId(id);
        return dto;
    }

    public static UserSubscriptionDto getUserSubscriptionDto(int customerId, int subscriptionId, int userId) {
        UserSubscriptionDto dto = new UserSubscriptionDto();
        dto.setCustomerId(customerId);
        dto.setSubscriptionId(subscriptionId);
        dto.setUserId(userId);
        return dto;

    }
}
