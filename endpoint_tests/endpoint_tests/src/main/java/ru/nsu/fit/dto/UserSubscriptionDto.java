package ru.nsu.fit.dto;

/**
 * Created by kirill on 17.11.2016.
 */
public class UserSubscriptionDto extends AbstractDto {

    int customerId;
    int subscriptionId;
    int userId;


    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(int subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
