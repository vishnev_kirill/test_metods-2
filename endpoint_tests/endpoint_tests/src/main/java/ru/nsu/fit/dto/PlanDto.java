package ru.nsu.fit.dto;

/**
 * Created by kirill on 12.11.2016.
 */
public class PlanDto extends AbstractDto {
    int id;
    String name;
    String details;
    int maxSeats;
    int freePerUnit;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getMaxSeats() {
        return maxSeats;
    }

    public void setMaxSeats(int maxSeats) {
        this.maxSeats = maxSeats;
    }


    public int getFreePerUnit() {
        return freePerUnit;
    }

    public void setFreePerUnit(int freePerUnit) {
        this.freePerUnit = freePerUnit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new StringBuffer("Name: ").append(this.getName()).
                append("Details ").append(this.getDetails()).
                append("MaxSeats  ").append(this.getMaxSeats()).append("MinSeats ").
                append("FreePerUnit").append(this.getFreePerUnit()).toString();
    }



    @Override
    public boolean equals(Object obj) {
        PlanDto dto = (PlanDto) obj;
        if(dto.getName().equals(this.getName()))
        {
            if(dto.getDetails().equals(this.getDetails())){
                if(dto.getMaxSeats() == this.getMaxSeats())
                {
                    if(dto.getFreePerUnit()==this.getFreePerUnit())

                            return true;
                    }
                }
            }

        return false;
    }
}

