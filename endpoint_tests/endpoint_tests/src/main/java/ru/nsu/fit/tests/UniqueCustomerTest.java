package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.utils.AllureUtils;
import ru.nsu.fit.utilsfortesting.ProviderService;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import java.io.IOException;

/**
 * Created by Алия on 19.11.2016.
 */
@Title("UniqueCustomerTest")
public class UniqueCustomerTest extends CreateCustomerTests {

    @Test(dependsOnMethods = {"checkCustomer"})
    @Step("Create customer again")
    @Description("Create customer again via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("revision  Unique  data feature")
    public void createCustomerAgain() throws IOException {
        WebTarget webTarget = ProviderService.getTarget("admin", "setup", "admin/addCustomer");
        String s = ProviderService.postDto(customerDto, webTarget);
        System.out.println("Создали ещё раз");
        Assert.assertEquals("Customer have been added already", s);
        AllureUtils.saveTextLog("Response: " + s);
    }


}

