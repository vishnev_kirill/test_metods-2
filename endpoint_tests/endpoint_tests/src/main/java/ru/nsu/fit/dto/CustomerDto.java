package ru.nsu.fit.dto;

/**
 * Created by kirill on 12.11.2016.
 */
public class CustomerDto extends AbstractDto implements LoginDto{

    int id;
    String firstname;
    String lastName;
    String login;
    String pass;
    int money;

    public CustomerDto(){

    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString(){
        return new StringBuffer("firstname: ").append(this.getFirstname()).
                append("LastName ").append(this.getLastName()).
                append("Login  ").append(this.getLogin()).append(" money").append(this.getMoney()).toString();
    }

    @Override
    public boolean equals(Object obj) {
        CustomerDto dto = (CustomerDto) obj;
        if(dto.getLogin().equals(this.getLogin()))
        {
            if(dto.getFirstname().equals(this.getFirstname())){
                if(dto.getMoney() == this.getMoney())
                {
                    if(dto.getLastName().equals(this.getLastName()))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}