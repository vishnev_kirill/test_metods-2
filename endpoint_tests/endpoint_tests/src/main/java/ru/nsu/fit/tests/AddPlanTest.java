package ru.nsu.fit.tests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import ru.nsu.fit.dto.PlanDto;
import ru.nsu.fit.utils.AllureUtils;
import ru.nsu.fit.utilsfortesting.DataGenerator;
import ru.nsu.fit.utilsfortesting.ProviderService;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import java.io.IOException;
import java.util.List;

/**
 * Created by kirill on 21.11.2016.
 */


public class AddPlanTest {

    protected PlanDto plan;

    @Test
    @Title("Create first plan")
    @Severity(SeverityLevel.CRITICAL)
    @Features("plan feature")
    @Step("Create plan")
    public void createPlan() throws IOException {
        WebTarget webTarget = ProviderService.getTarget("admin", "setup", "admin/addPlan");
        plan= DataGenerator.getPlanDto();
        String s = ProviderService.postDto(plan, webTarget);
        System.out.println("добавили план");
        Assert.assertEquals("OK", s);
        AllureUtils.saveTextLog("first plan" + s);
    }

    @Test(dependsOnMethods = {"createPlan"})
    @Title("Check Plan")
    @Description("Check added paln via REST API")
    @Severity(SeverityLevel.CRITICAL)
    @Step("Сheck plan")
    @Features("plan feature")
    public void CheckPlans() throws IOException {
        WebTarget webTarget = ProviderService.getTarget("1", "2", "all/getPlans");
        String plans = ProviderService.get(webTarget);
        ObjectMapper m = new ObjectMapper();
        List<PlanDto> list = m.readValue(plans, new TypeReference<List<PlanDto>>() {});

        boolean flag=false;
        for (PlanDto d : list) {
            if (d.equals(plan))
                flag=true;



        }
        Assert.assertEquals(true, flag);
        System.out.println("Проверили план");
    }


    @AfterClass
    @Title("dell plan")
    @Step("dellete plan")
    @Severity(SeverityLevel.BLOCKER)
    @Features("plan feature")
    public void dellPlan() throws IOException {
        WebTarget webTarget = ProviderService.getTarget("admin", "setup","admin/dellPlan");
        String s = ProviderService.postString(plan.getName(),webTarget);
        System.out.println("Удалили план");
        AllureUtils.saveTextLog("dellete plan" + s);

    }
}
