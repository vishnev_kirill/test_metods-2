package ru.nsu.fit.dto;

/**
 * Created by kirill on 16.11.2016.
 */
public class BuyDto extends AbstractDto {

    int customerId;
    int planId;


    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }
}
