package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.utils.AllureUtils;
import ru.nsu.fit.utilsfortesting.ProviderService;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import java.io.IOException;

/**
 * Created by Алия on 19.11.2016.
 */

@Title("UniquePlanTest")
public class UniquePlanTest extends AddPlanTest {


    @Test(dependsOnMethods = {"createPlan"})
    @Step("Create plan again")
    @Description("Create customer via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Unique plan feature")
    public void createPlanAgain() throws IOException {
        WebTarget webTarget = ProviderService.getTarget("admin", "setup","admin/addPlan");
        String s = ProviderService.postDto(plan,webTarget);
        System.out.println("Добавили план ещё раз");
        Assert.assertEquals("Plan have been added already",s);
        AllureUtils.saveTextLog("Response: " + s);
    }


}
