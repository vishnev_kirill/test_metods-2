package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.dto.DepositDto;
import ru.nsu.fit.utils.AllureUtils;
import ru.nsu.fit.utilsfortesting.DataGenerator;
import ru.nsu.fit.utilsfortesting.ProviderService;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import java.io.IOException;

/**
 * Created by Алия on 19.11.2016.
 */
@Test(priority = 5)
@Title("DepositNegativeTest")
public class DepositNegativeTest extends BaseCustomerTests {


    @Test
    @Title("get negative deposit")
    @Step("get negative deposit")
    @Description("Change Deposit via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Deposit feature")
    public void getDepositPositive() throws IOException {
        WebTarget webTarget = ProviderService.getTarget(customerDto.getLogin(),customerDto.getPass(),"customer/addMoney");
        DepositDto dto = DataGenerator.getDepositDto(customer_id, -100);
        String s = ProviderService.postDto(dto,webTarget);
        System.out.println("Пополнили баланс отрицательным числом" +s);
        AllureUtils.saveTextLog("Server say " + s);
        Assert.assertEquals("money<0", s);
    }
}