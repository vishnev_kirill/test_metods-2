package ru.nsu.fit.tests;

/**
 * Created by Алия on 19.11.2016.
 */

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.utilsfortesting.ProviderService;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import java.io.IOException;

@Title("UniqueUserTest")
public class UniqueUserTest extends CreateUserTest {




    @Test
    @Step("Create User Again")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Unique feature")
    public void createUserAgain() throws IOException {
        WebTarget webTarget = ProviderService.getTarget(customerDto.getLogin(),customerDto.getPass(),"customer/addUser");
        String s = ProviderService.postDto(user,webTarget);
        System.out.println("Создали юзера ещё раз");
        Assert.assertEquals("User have been added already", s);
    }


}
