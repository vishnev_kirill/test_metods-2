package ru.nsu.fit.utilsfortesting;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import ru.nsu.fit.dto.*;
import ru.nsu.fit.utils.AllureUtils;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

/**
 * Created by Алия on 18.11.2016.
 */
public class ProviderService {

    public static final String PLAN_NAME1 = "Plan1";
    public static final String PLAN_NAME2= "Plan2";
    public static final String PLAN_NAME3 = "!Plan3";

    public static WebTarget getTarget(String login, String password, String path) {
        ResteasyClient client = new ResteasyClientBuilder().build().register(new Authenticator(login, password));
        return client.target("http://localhost:8080/endpoint/rest").path(path);
    }

    public static String get(WebTarget webTarget) {
        Response resp = webTarget.request().get();
        return resp.readEntity(String.class);
    }

    public static String postDto(AbstractDto dto, WebTarget webTarget) throws IOException {
        ObjectMapper m = new ObjectMapper();
        String data = m.writeValueAsString(dto);
        Response resp = webTarget.request().post(Entity.entity(data, "application/json"));
        return resp.readEntity(String.class);
    }

    public static String postString(String s, WebTarget webTarget) throws IOException{
        Response resp = webTarget.request().post(Entity.entity(s, "application/json"));
        return resp.readEntity(String.class);
    }

/* public static <T extends LoginDto> LoginDto findByLogin( String data, String login) throws IOException{
        ObjectMapper m = new ObjectMapper();
        List<T > list = m.readValue(data, new TypeReference<List<T>>() {
        });

        for (LoginDto d : list){
            if(d.getLogin().equals(login)){
                return d;
            }
        }
        return null;
    }*/


public static void createTestGroupPlans() throws IOException {
    PlanDto dto = new PlanDto();
    PlanDto dto1 = new PlanDto();
    PlanDto dto2 = new PlanDto();

    dto.setId(0);
    dto1.setId(0);
    dto2.setId(0);

    dto.setName(PLAN_NAME1);
    dto1.setName(PLAN_NAME2);
    dto2.setName(PLAN_NAME3);

    dto.setDetails("good Plan");
    dto1.setDetails("vey good Plan");
    dto2.setDetails("very very good Plan");

    dto.setMaxSeats(10);
    dto1.setMaxSeats(15);
    dto2.setMaxSeats(10);

    dto.setFreePerUnit(20);
    dto1.setFreePerUnit(30);
    dto2.setFreePerUnit(40);

    WebTarget target= getTarget("admin","setup","admin/addPlan");
    postDto(dto,target);
    postDto(dto1,target);
    postDto(dto2,target);




}

public static List<PlanDto> getPlans() throws IOException {

    WebTarget webTarget = getTarget("1", "2","all/getPlans");
    String plans = ProviderService.get(webTarget);
    ObjectMapper m = new ObjectMapper();
    System.out.println("Получили планы");
    return m.readValue(plans, new TypeReference<List<PlanDto>>() {});
}

public static void dellPlans(List<PlanDto> plans) throws IOException{

    WebTarget webTarget = ProviderService.getTarget("admin", "setup", "admin/dellPlan");


    for(PlanDto dto : plans) {
        String s = ProviderService.postString(dto.getName(), webTarget);
        AllureUtils.saveTextLog("dellete plan" + s);
    }

    System.out.println("Удалили планы");
}




}
