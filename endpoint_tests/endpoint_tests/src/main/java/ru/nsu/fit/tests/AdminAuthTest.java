package ru.nsu.fit.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.dto.UserDto;
import ru.nsu.fit.utils.AllureUtils;
import ru.nsu.fit.utilsfortesting.DataGenerator;
import ru.nsu.fit.utilsfortesting.ProviderService;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import java.io.IOException;

/**
 * Created by kirill on 14.11.2016.
 */

public class AdminAuthTest {


    @Test
    @Title("Create user ")
    @Step("Creating user with admin")
    @Description("Create user via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Authentication feature")
    public void createUser() throws IOException {
        WebTarget webTarget = ProviderService.getTarget("admin", "setup","customer/addUser");
        UserDto dto = DataGenerator.getUserDto();
        String s = ProviderService.postDto(dto,webTarget);
        System.out.println("Создали с admin");
        Assert.assertEquals("You cannot access this resource",s);
        AllureUtils.saveTextLog("status " + s);
    }

}
