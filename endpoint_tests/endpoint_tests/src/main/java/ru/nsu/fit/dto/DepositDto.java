package ru.nsu.fit.dto;

/**
 * Created by kirill on 16.11.2016.
 */
public class DepositDto extends AbstractDto {

    int id;
    int money;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }
}
