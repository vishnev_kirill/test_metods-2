package ru.nsu.fit.dto;

/**
 * Created by kirill on 15.11.2016.
 */
public class ChangeRoleDto extends AbstractDto {
    int customerId;
    String login;
    String newRole;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNewRole() {
        return newRole;
    }

    public void setNewRole(String newRole) {
        this.newRole = newRole;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

}
