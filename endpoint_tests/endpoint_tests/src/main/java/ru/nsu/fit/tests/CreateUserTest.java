package ru.nsu.fit.tests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.dto.UserDto;
import ru.nsu.fit.utils.AllureUtils;
import ru.nsu.fit.utilsfortesting.DataGenerator;
import ru.nsu.fit.utilsfortesting.ProviderService;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import java.io.IOException;
import java.util.List;

/**
 * Created by kirill on 23.11.2016.
 */

public class CreateUserTest extends BaseCustomerTests {

    protected UserDto user;
    protected int userId;

    @BeforeClass
    @Title("Create user")
    @Step("Create user")
    @Description("Create user via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Authentication feature")
    public void createUser() throws IOException {
        WebTarget webTarget = ProviderService.getTarget(customerDto.getLogin(),customerDto.getPass(),"customer/addUser");
        UserDto dto = DataGenerator.getUserDto();
        dto.setCustomeId(customer_id);
        user = dto;
        String s = ProviderService.postDto(user,webTarget);
        System.out.println("заводим юзера");
        Assert.assertEquals("OK", s);
        AllureUtils.saveTextLog("Status: " + s);
    }

    @Test
    @Title("Check data user")
    @Step("Check data")
    @Description("Check User via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Authentication feature")
    public void checkUser() throws IOException {
        WebTarget webTarget = ProviderService.getTarget(customerDto.getLogin(),customerDto.getPass(),"customer/getMyUsers");
        String users = ProviderService.postString(Integer.toString(customer_id),webTarget);
        ObjectMapper m = new ObjectMapper();
        List<UserDto> list = m.readValue(users, new TypeReference<List<UserDto>>() {
        });

        boolean flag=false;
        UserDto userDto = new UserDto();
        for (UserDto d : list){
            if(d.equals(user)){
                userId=d.getId();
                flag=true;
            }
        }
        System.out.println(user.toString());
        System.out.println("Проверили создание юзера");
        Assert.assertEquals(true,flag);
        System.out.println(flag);

        AllureUtils.saveTextLog("Status: " + flag);
    }


    @AfterClass
    @Step("dell user")
    @Description("dell user again via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Basic Customer feature")
    public void dellUser() throws IOException {

        WebTarget t=ProviderService.getTarget(user.getLogin(),user.getPass(),"user/getId");
        String str=ProviderService.postString(user.getLogin(),t);

        WebTarget webTarget = ProviderService.getTarget(customerDto.getLogin(),customerDto.getPass(),"customer/dellUser");
        String s = ProviderService.postString(str,webTarget);
        System.out.println("Удалили юзера " + s);
        AllureUtils.saveTextLog("Response: " + s);
    }
}
