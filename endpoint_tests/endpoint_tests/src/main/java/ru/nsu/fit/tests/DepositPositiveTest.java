package ru.nsu.fit.tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.dto.CustomerDto;
import ru.nsu.fit.dto.DepositDto;
import ru.nsu.fit.utils.AllureUtils;
import ru.nsu.fit.utilsfortesting.DataGenerator;
import ru.nsu.fit.utilsfortesting.ProviderService;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import java.io.IOException;

/**
 * Created by Алия on 19.11.2016.
 */

@Title("DepositPositiveTest")
public class DepositPositiveTest extends BaseCustomerTests {

      private int oldМoney=0;
      private int posMoney=100;


    @BeforeClass
    @Step("get customer money")
    @Description("Change deposit via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Deposit feature")
    public void getMoney() throws IOException {
        WebTarget webTarget = ProviderService.getTarget(customerDto.getLogin(),customerDto.getPass(),"customer/getMyData");
        String s = ProviderService.postString(String.valueOf(customer_id),webTarget);
        System.out.println("узнали счёт");
        ObjectMapper m = new ObjectMapper();
        CustomerDto customer = m.readValue(s,CustomerDto.class);
        oldМoney = customer.getMoney();
        System.out.println(oldМoney);
        AllureUtils.saveTextLog("Сustomer have" + oldМoney);
    }



    @Test
    @Step("get positive deposit")
    @Description("Change deposit via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Deposit feature")
    public void getDepositPositive() throws IOException {
        WebTarget webTarget = ProviderService.getTarget(customerDto.getLogin(),customerDto.getPass(),"customer/addMoney");
        DepositDto dto = DataGenerator.getDepositDto(customer_id, posMoney);
        String s = ProviderService.postDto(dto,webTarget);
        System.out.println("Пополнили на " + oldМoney );
        AllureUtils.saveTextLog("Status: " + s + " + " + posMoney +"  $");
        Assert.assertEquals("OK", s);
    }


    @Test(dependsOnMethods = {"getDepositPositive"})
    @Step("get customer money")
    @Description("Create user via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Deposit feature")
    public void getMoneyAgain() throws IOException {
        WebTarget webTarget = ProviderService.getTarget(customerDto.getLogin(),customerDto.getPass(),"customer/getMyData");
        String s = ProviderService.postString(String.valueOf(customer_id),webTarget);
        ObjectMapper m = new ObjectMapper();
        CustomerDto customer = m.readValue(s,CustomerDto.class);
        int newMoney = customer.getMoney();
        System.out.println(newMoney);
        System.out.println("Имеем " + (oldМoney+posMoney));
        AllureUtils.saveTextLog(" Customer have " + newMoney);
        Assert.assertEquals(oldМoney+posMoney, newMoney);
    }

}
