package ru.nsu.fit.tests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.dto.SubscriptionDto;
import ru.nsu.fit.dto.UserSubscriptionDto;
import ru.nsu.fit.utils.AllureUtils;
import ru.nsu.fit.utilsfortesting.DataGenerator;
import ru.nsu.fit.utilsfortesting.ProviderService;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import java.io.IOException;
import java.util.List;

/**
 * Created by kirill on 24.11.2016.
 */
public class AddUserOnSubscriptionTest extends SubscriptionTest {

    @Test(dependsOnMethods = {"checkPlan", "checkUser"})
    @Step("Add user on Subscription")
    @Description("add user on paln via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Subscription feature")
    public void addUserOnSubscription() throws IOException {
        WebTarget webTarget = ProviderService.getTarget(customerDto.getLogin(), customerDto.getPass(), "customer/addUserInSubs");
        UserSubscriptionDto dto = DataGenerator.getUserSubscriptionDto(customer_id,subscription.getId(),userId);
        String req = ProviderService.postDto(dto, webTarget);
        AllureUtils.saveTextLog("Status " + req);
        //System.out.println("Подписались на план");
        Assert.assertEquals(req, "Plan added");
    }


    @Test(dependsOnMethods = {"addUserOnSubscription"})
    @Step("Add user on Subscription")
    @Description("add user on paln via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Subscription feature")
    public void check() throws IOException {
        WebTarget webTarget = ProviderService.getTarget(user.getLogin(), user.getPass(), "user/getMySubscriptions");
        String req = ProviderService.postString(Integer.toString(userId),webTarget);
        ObjectMapper m = new ObjectMapper();
        List<SubscriptionDto> list = m.readValue(req, new TypeReference<List<SubscriptionDto>>() {});

        boolean flag=false;
        for (SubscriptionDto d : list){
            if(d.getId()==subscription.getId())
                flag=true;

        }

        AllureUtils.saveTextLog("user added" + flag);
        System.out.println("проверили подписку на план");
        Assert.assertEquals(flag, true);
    }

}
