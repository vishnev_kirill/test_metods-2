package ru.nsu.fit.dto;

/**
 * Created by kirill on 12.11.2016.
 */
public class UserDto extends AbstractDto implements LoginDto{
    int id;
    String firstname;
    String lastName;
    String login;
    String pass;
    String role;
    int customeId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getCustomeId() {
        return customeId;
    }

    public void setCustomeId(int customeId) {
        this.customeId = customeId;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }



    @Override
    public String toString(){
        return new StringBuffer("firstname: ").append(this.getFirstname()).
                append("LastName ").append(this.getLastName()).
                append("Login  ").append(this.getLogin()).append("role ").append(this.getRole()).toString();
    }

    @Override
    public boolean equals(Object obj) {
        UserDto dto = (UserDto) obj;
        if(dto.getLogin().equals(this.getLogin()))
        {
            if(dto.getFirstname().equals(this.getFirstname())){
                if(dto.getCustomeId() == this.getCustomeId())
                {
                    if(dto.getLastName().equals(this.getLastName()))
                    {
                        if(dto.getRole().equals(this.getRole()))
                            return true;
                    }
                }
            }
        }
        return false;
    }
}
