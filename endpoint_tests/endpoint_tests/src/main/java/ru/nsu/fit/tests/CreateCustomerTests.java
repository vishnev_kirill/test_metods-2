package ru.nsu.fit.tests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.dto.CustomerDto;
import ru.nsu.fit.utils.AllureUtils;
import ru.nsu.fit.utilsfortesting.DataGenerator;
import ru.nsu.fit.utilsfortesting.ProviderService;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.WebTarget;
import java.io.IOException;
import java.util.List;

/**
 * Created by kirill on 22.11.2016.
 */

@Title("first suite")
@Test
public class CreateCustomerTests {

    protected CustomerDto customerDto;

    @BeforeClass
    @Title("Create customer")
    @Description("Create customer via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Step("Create Customer")
    @Features("Basic Admin feature")
    protected void createCustomer() throws IOException {
        WebTarget webTarget = ProviderService.getTarget("admin", "setup","admin/addCustomer");
        CustomerDto dto = DataGenerator.getCustomerDto();
        customerDto = dto;
        System.out.println("Создали");
        String s = ProviderService.postDto(dto,webTarget);
        Assert.assertEquals("OK",s);
        AllureUtils.saveTextLog("Customer added " + s);
    }

    @Test
    @Title("Check data customer")
    @Step("Check correct customer")
    @Description("Check customer via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Basic Admin feature")
    protected void checkCustomer() throws IOException {
        WebTarget webTarget = ProviderService.getTarget("admin", "setup","admin/getCustomers");
        String customers = ProviderService.get(webTarget);
        ObjectMapper m = new ObjectMapper();
        System.out.println("Проверили");
        List<CustomerDto> list = m.readValue(customers, new TypeReference<List<CustomerDto>>() {});

        boolean flag=false;
        CustomerDto curCust = new CustomerDto();
        for (CustomerDto d : list){
            if(d.equals(customerDto)) {
                curCust=d;
                flag=true;
            }
        }

        Assert.assertEquals(true,flag);
        AllureUtils.saveTextLog("data correct" + flag);
        String p=customerDto.getPass();
        customerDto=curCust;
        customerDto.setPass(p);
    }

    @AfterClass
    @Step("Delete customer")
    public  void dellCustomer() throws IOException{

        WebTarget t=ProviderService.getTarget(customerDto.getLogin(),customerDto.getPass(),"customer/getId");
        String str=ProviderService.postString(customerDto.getLogin(),t);

        WebTarget webTarget = ProviderService.getTarget("admin","setup","admin/dellCustomer");
        String s = ProviderService.postString(str,webTarget);
        System.out.println("удалили");
        AllureUtils.saveTextLog("Response: " + s);
    }





}
